package com.example.azzarnoy.cw_maps;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    final LatLng KHARKOV = new LatLng(49.9944422,36.2368201);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this); //getting map


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        Marker marker = googleMap.addMarker(new MarkerOptions().position(KHARKOV).title("Kharkov")); //create marker
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(KHARKOV, 15)); // Move the camera instantly to Kharkov with a zoom of 15.
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null); // Zoom in, animating the camera.
    }
}
